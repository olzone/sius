var isSerwerOK = true
var users = []

//var isAuth = false

var room = false
var nick = false

var params = location.search && location.search && location.search.split('?')[1];
var sparams = params.split('&');
for(var i=0;i<sparams.length;i++)
{
	if(sparams[i].startsWith("room="))
		room = sparams[i].replace("room=","");
	if(sparams[i].startsWith("nick="))
		nick = sparams[i].replace("nick=","");
}	
updateUsers();

if(!nick)
	window.location.replace("index.html");
else if(isSerwerOK)
{
	jQuery.ajaxSetup({async:false});

	$.soap({
		url: 'https://sius.azurewebsites.net/services/roomServer/',
		method: 'isUsernameTaken',
		
		namespaceQualifier: 'sius',
		namespaceURL: 'http://sius.azurewebsites.net',

		data: {
			name: nick,
		},
		
		headers: {
			'Access-Control-Allow-Origin' : '*',
		},

		success: function (r) {
			var res = r.toJSON()['#document']['soapenv:Envelope']['soapenv:Body']['ns:isUsernameTakenResponse']['ns:return']; //kogo popier****o ?!?!?!?!
			
			/////////////////////nick nie jest dostepny
			if(res == 'true')
			{
				bootbox.alert("username is taken! try another one", function() {
					window.location.replace("index.html");
				});
			}
			else /////////////////////nick jest dostepny
			{
				
				/////////////////////blokujemy nick
				$.soap({
					url: 'https://sius.azurewebsites.net/services/roomServer/',
					method: 'registerUsername',
					
					namespaceQualifier: 'sius',
					namespaceURL: 'http://sius.azurewebsites.net',

					data: {
						name: nick,
					},
					
					headers: {
						'Access-Control-Allow-Origin' : '*',
					},
				});
				if(room)
				{
					$.soap({
						url: 'https://sius.azurewebsites.net/services/roomServer/',
						method: 'isRoomPasswordProtected',
						
						namespaceQualifier: 'sius',
						namespaceURL: 'http://sius.azurewebsites.net',

						data: {
							roomName: room,
						},
						
						headers: {
							'Access-Control-Allow-Origin' : '*',
						},

						success: function (r) {
							console.log(r)
							var res = r.toJSON()['#document']['soapenv:Envelope']['soapenv:Body']['ns:isRoomPasswordProtectedResponse']['ns:return']; //kogo popier****o ?!?!?!?!
							
							if(!(res == "Room does not exist" || res == "false"))
							///////////////////// Pokoj ma haslo
							{
								bootbox.prompt("Password-Protected Room. Type password:", function(result) {                
								  if (result === null) { 
									window.onbeforeunload();
									window.location.replace(location.pathname+"?nick="+nick);                          
								  } else {
									 if(result != res)		
										bootbox.alert("Wrong password!", function() {
											window.onbeforeunload();
											window.location.replace(location.pathname+"?nick="+nick);                          
										});									 
									//isAuth = true
								//$('#otherPeopleVideo').each(function() {
								//	$(this).get(0).autoplay = true;
								//	$(this).get(0).load();
								//});
									webrtc.startLocalVideo();	
									
									document.getElementById("addRoomPsw").style.display = "none";
									document.getElementById("remRoomPsw").style.removeProperty('display');
									
									$.soap({
										url: 'https://sius.azurewebsites.net/services/roomServer/',
										method: 'findMessgaesByRoomAndDate',
										
										namespaceQualifier: 'sius',
										namespaceURL: 'http://sius.azurewebsites.net',

										data: {
											room: room ? room : "GlobalRoom",
											date: new Date().getTime() + 100000000000,
										},
										
										headers: {
											'Access-Control-Allow-Origin' : '*',
										},

										success: function (r) {
											var res = r.toJSON()['#document']['soapenv:Envelope']['soapenv:Body']['ns:findMessgaesByRoomAndDateResponse']['ns:return']; //kogo popier****o ?!?!?!?!
											console.log(res);
											if(res != undefined)
											for(var i = 0; i<res.length; i++)
												document.getElementById('chatTextarea').innerHTML += buildMsgWithTime(res[i]['ax21:user'], res[i]['ax21:message'], new Date(parseInt(res[i]['ax21:date']['time']['_'])));
										},
										error: function (r) {
											console.log(r);
										}
									});
									}
								});
							}
							else
							{
								//$('#otherPeopleVideo').each(function() {
								//	$(this).get(0).autoplay = true;
								//	$(this).get(0).load();
								//});
								webrtc.startLocalVideo();	
								//isAuth = true;
							
								
							
								///////////////////// Ladujemy historie dla pokoju
								$.soap({
									url: 'https://sius.azurewebsites.net/services/roomServer/',
									method: 'findMessgaesByRoomAndDate',
									
									namespaceQualifier: 'sius',
									namespaceURL: 'http://sius.azurewebsites.net',

									data: {
										room: room ? room : "GlobalRoom",
										date: new Date().getTime() + 100000000000,
									},
									
									headers: {
										'Access-Control-Allow-Origin' : '*',
									},

									success: function (r) {
										var res = r.toJSON()['#document']['soapenv:Envelope']['soapenv:Body']['ns:findMessgaesByRoomAndDateResponse']['ns:return']; //kogo popier****o ?!?!?!?!
										console.log(res);
										if(res != undefined)
										for(var i = 0; i<res.length; i++)
											document.getElementById('chatTextarea').innerHTML += buildMsgWithTime(res[i]['ax21:user'], res[i]['ax21:message'], new Date(parseInt(res[i]['ax21:date']['time']['_'])));
									},
									error: function (r) {
										console.log(r);
									}
								});
							}
							/*if(res != 'false')
							{
								bootbox.alert("hasło!", function() {
									window.location.replace("index.html");
								});
							}*/
						},
						error: function (r) {
							console.log(r);
						}
					});
				}
				else
				{
					///////////////////// Ladujemy historie dla hallu
					$.soap({
						url: 'https://sius.azurewebsites.net/services/roomServer/',
						method: 'findMessgaesByRoomAndDate',
						
						namespaceQualifier: 'sius',
						namespaceURL: 'http://sius.azurewebsites.net',

						data: {
							room: room ? room : "GlobalRoom",
							date: new Date().getTime() + 100000000000,
						},
						
						headers: {
							'Access-Control-Allow-Origin' : '*',
						},

						success: function (r) {
							var res = r.toJSON()['#document']['soapenv:Envelope']['soapenv:Body']['ns:findMessgaesByRoomAndDateResponse']['ns:return']; //kogo popier****o ?!?!?!?!
							console.log(res);
							for(var i = 0; i<res.length; i++)
								document.getElementById('chatTextarea').innerHTML += buildMsgWithTime(res[i]['ax21:user'], res[i]['ax21:message'], new Date(parseInt(res[i]['ax21:date']['time']['_'])));
						},
						error: function (r) {
							console.log(r);
						}
					});
				}
			}
		},
		error: function (r) {
			console.log(r);
		}
	});

	jQuery.ajaxSetup({async:true});
}

//******************************************//
var stun = {
	'url': 'stun:stun1.l.google.com:19302'
};

var turn = {
	'url': 'turn:137.135.200.164:3478',
	'username': 'plok', 
	'credential': 'plok'
}; 
//http://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/
//******************************************//


function showVolume(el, volume) {
	if (!el) return;
	if (volume < -45) volume = -45;
	if (volume > -20) volume = -20;
	el.value = volume;
}


var webrtc = new SimpleWebRTC({
	url: "https://137.135.200.164:8888/", //Serwer do signalizowania
	localVideoEl: 'localVideo',
	remoteVideosEl: '',
	socketio: {'force new connection':true},
	signalingOptions: {'force new connection':true},
	nick: nick,
	autoRequestMedia: false,	
	enableDataChannels: true,
	/*media: {video: true, audio: true},
	receiveMedia: {
        mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        }
    }*/
});

webrtc.on('readyToCall', function () {
	console.log("ready!!!");
	if (room) webrtc.joinRoom(room);
	//else webrtc.joinRoom("GlobalRoom");
});

webrtc.on('connectionReady', function (sessionId) {
	console.log("MY ID: "+ webrtc.connection.connection.id)
	if (!room) webrtc.joinRoom("GlobalRoom");
	
})

window.onbeforeunload = function (event) {
    webrtc.sendDirectlyToAll("sayGoodbye", "sayGoodbye", nick);
	if(nick && isSerwerOK)
	$.soap({
		url: 'https://sius.azurewebsites.net/services/roomServer/',
		method: 'unregisterUsername',
		
		namespaceQualifier: 'sius',
		namespaceURL: 'http://sius.azurewebsites.net',

		data: {
			name: nick,
		},
		
		headers: {
			'Access-Control-Allow-Origin' : '*',
		},
	});
};

webrtc.on('volumeChange', function (volume, treshold) {
	showVolume(document.getElementById('localVolume'), volume);
});

webrtc.on('remoteVolumeChange', function (peer, volume) {
	showVolume(document.getElementById('volume_' + peer.id), volume);
});

webrtc.on('createdPeer', function (peer) {
	setTimeout(function()
	{
		console.log(peer.nick);
		users.push(peer.nick);
		updateUsers();
		//webrtc.sendDirectlyToAll("seyHello", "seyHello", nick);
		
	}, 1000);
	
	var largeVideoBox = document.getElementById('largeVideoBox');
	
	var container = document.createElement('div');
    container.className = 'peerContainer';
    container.id = 'files_container_' + webrtc.getDomId(peer);
	container.style.display = "none";
	
	var filelist = document.createElement('ul');
    filelist.className = 'fileList';
    container.appendChild(filelist);
	
	
	var labelf = document.createElement('label');
	labelf.appendChild(document.createTextNode("Choose file"));
	labelf.className = "btn btn-success btn-file";
	var fileinput = document.createElement('input');
	fileinput.style.display = "none";
    fileinput.type = 'file';
	
		// send a file
	fileinput.addEventListener('change', function() {
        fileinput.disabled = true;

        var file = fileinput.files[0];
        var sender = peer.sendFile(file);

        // create a file item
        var item = document.createElement('li');
        item.className = 'sending';

        // make a label
        var span = document.createElement('span');
        span.className = 'filename';
        span.appendChild(document.createTextNode(file.name));
        item.appendChild(span);

        span = document.createElement('span');
        span.appendChild(document.createTextNode(file.size + ' bytes'));
        item.appendChild(span);

        // create a progress element
        var sendProgress = document.createElement('progress');
        sendProgress.max = file.size;
        item.appendChild(sendProgress);

        // hook up send progress
        sender.on('progress', function (bytesSent) {
            sendProgress.value = bytesSent;
        });
        // sending done
        sender.on('sentFile', function () {
            item.appendChild(document.createTextNode('sent'));

            // we allow only one filetransfer at a time
            fileinput.removeAttribute('disabled');
        });
        // receiver has actually received the file
        sender.on('complete', function () {
            // safe to disconnect now
        });
        filelist.appendChild(item);
    }, false);
    //fileinput.disabled = 'disabled';
	labelf.appendChild(fileinput);
    container.appendChild(labelf);

    // receiving an incoming filetransfer
    peer.on('fileTransfer', function (metadata, receiver) {
        console.log('incoming filetransfer', metadata);
        var item = document.createElement('li');
        item.className = 'receiving';

        // make a label
        var span = document.createElement('span');
        span.className = 'filename';
        span.appendChild(document.createTextNode(metadata.name));
        item.appendChild(span);

        span = document.createElement('span');
        span.appendChild(document.createTextNode(metadata.size + ' bytes'));
        item.appendChild(span);

        // create a progress element
        var receiveProgress = document.createElement('progress');
        receiveProgress.max = metadata.size;
        item.appendChild(receiveProgress);

        // hook up receive progress
        receiver.on('progress', function (bytesReceived) {
            receiveProgress.value = bytesReceived;
        });
        // get notified when file is done
        receiver.on('receivedFile', function (file, metadata) {
            console.log('received file', metadata.name, metadata.size);
            var href = document.createElement('a');
            href.href = URL.createObjectURL(file);
            href.download = metadata.name;
            href.appendChild(document.createTextNode('download'));
            item.appendChild(href);

            // close the channel
            receiver.channel.close();
        });
        filelist.appendChild(item);
    });
	largeVideoBox.appendChild(container);
	
	
});

webrtc.on('videoAdded', function (video, peer) {
	console.log("videoAdded")
	if(!room)
		{
			users.push(peer.nick);
			updateUsers();
			return;
		}
	
	console.log('video added', peer);
	var remotes = document.getElementById('remotes');
	if (remotes) {
		var supercontainer = document.createElement('li');
		var container = document.createElement('div');
		var nickcontainer = document.createElement('div');
		var nicktxt = document.createTextNode(peer.nick);
		supercontainer.appendChild(container);
		supercontainer.className = "col-lg-12 col-md-12 col-sm-12 col-xs-12";
		//supercontainer.id = "videoBox";
		supercontainer.id = 'container_' + webrtc.getDomId(peer);
		supercontainer.addEventListener('click', function (event) {
				funcusOnThisVideo(supercontainer, peer);
		});

		container.appendChild(nickcontainer); 
		nickcontainer.appendChild(nicktxt); 
		nickcontainer.className = "video_nickcontainer";
		console.log(peer.nick);
		container.className = 'videoContainer';
		container.id = 'container_' + webrtc.getDomId(peer);
		container.appendChild(video);
		video.id = "otherPeopleVideo";
		video.oncontextmenu = function () { return false; };
		video.onpause = function () { video.load()};
		//if(!isAuth)
		//	video.autoplay = false;
		
		/*
		var vol = document.createElement('meter');
		vol.id = 'volume_' + peer.id;
		vol.className = 'volume';
		vol.min = -45;
		vol.max = -20;
		vol.low = -40;
		vol.high = -25;
		container.appendChild(vol);
		*/
		if (peer && peer.pc) {
			var connstate = document.createElement('div');
			connstate.className = 'connectionstate';
			container.appendChild(connstate);
			peer.pc.on('iceConnectionStateChange', function (event) {
				switch (peer.pc.iceConnectionState) {
				case 'checking':
					connstate.innerText = 'Connecting to peer...';
					break;
				case 'connected':
				case 'completed': // on caller side
					connstate.innerText = 'Connection established.';
					break;
				case 'disconnected':
					connstate.innerText = 'Disconnected.';
					break;
				case 'failed':
					break;
				case 'closed':
					connstate.innerText = 'Connection closed.';
					break;
				}
			});
		}
		remotes.appendChild(supercontainer);
	}
});

webrtc.on('videoRemoved', function (video, peer) {
	if(!room)
	{
		users.splice(users.indexOf(peer.nick), 1);
		updateUsers();
		return;
	}
	//console.log('video removed ', peer);
	
	var largeVideoBox = document.getElementById('largeVideoBox')

	if (largeVideoBox) 
	{
		try {
			var el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			while(el)
			{
				console.log(el);
				largeVideoBox.removeChild(el);
				el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			}
		}
		catch(err) {
			;
		}
		
		try {
			var el = document.getElementById(peer ? 'files_container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			while(el)
			{
				console.log(el);
				largeVideoBox.removeChild(el);
				el = document.getElementById(peer ? 'files_container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			}
		}
		catch(err) {
			;
		}
	}
	
	var remotes = document.getElementById('remotes');

	if (remotes) 
	{
		try {
			var el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			while(el)
			{
				console.log(el);
				remotes.removeChild(el);
				el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer')
			}
		}
		catch(err) {
			;
		}
	}

});

// local p2p/ice failure
webrtc.on('iceFailed', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});

// remote p2p/ice failure
webrtc.on('connectivityError', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});

webrtc.on('stunservers', function (args) {
	if(args.length == 0)
		console.log("Brak serwera stun");
	else
	args.forEach(function(arg) {
		console.log("Serwer stun: " + arg.url);
	});
});

webrtc.on('turnservers', function (args) {
	if(args.length == 0)
		console.log("Brak serwera turn");
	else
	args.forEach(function(arg) {
		console.log("Serwer turn: " + arg.urls);
	});
});

function buildMsg(nick, msg){
	var time = new Date();
	return "<li><div class=\"chat-body clearfix\"><div class=\"header\"><strong class=\"primary-font\">"
	+nick+"</strong> <small class=\"pull-right text-muted\"><span class=\"glyphicon glyphicon-time\"></span>"
	+('0' + time.getHours()).slice(-2) + "  :  " + ('0' + time.getMinutes()).slice(-2) + "  :  " + ('0' + time.getSeconds()).slice(-2) +"</small></div><p>"
	+msg+"</p></div></li>";

}

function buildMsgWithTime(nick, msg, t){
	var time = new Date();
	return "<li><div class=\"chat-body clearfix\"><div class=\"header\"><strong class=\"primary-font\">"
	+nick+"</strong> <small class=\"pull-right text-muted\"><span class=\"glyphicon glyphicon-time\"></span>"
	+('0' + t.getHours()).slice(-2) + "  :  " + ('0' + t.getMinutes()).slice(-2) + "  :  " + ('0' + t.getSeconds()).slice(-2) +"</small></div><p>"
	+msg+"</p></div></li>";

}

webrtc.on('channelMessage', function (peer, label, data){
	console.log(data);
	if (label == "chat")
		document.getElementById('chatTextarea').innerHTML += buildMsg(data.payload.nick, data.payload.message);
	if (label == "sayGoodbye")
	{
		users.splice(users.indexOf(peer.nick), 1);
		updateUsers();
	}
	var elem = document.getElementById('chatTextarea');
	elem.scrollTop = elem.scrollHeight;

});

document.getElementById("firstJumbotron").style.display = "none";
document.getElementById("largeVideoBoxPanel").style.display = "none";


if(room)
{
	document.getElementById("remRoomPsw").style.display = "none";
	document.getElementById("firstJumbotron").style.display = "block";
	document.getElementById("largeVideoBoxPanel").style.display = "block";

	document.getElementById('roomName').innerHTML = "You are in <b>"+room+ "</b>"
	document.getElementById('localVid').style.display = "block"//"initial";
	document.getElementById('connectionstate').style.display = "block";
	document.getElementById('remotes').style.display = "block";
	//document.getElementById('goToRoom').innerHTML = "<p style=\"font-size:20px\"><a href="+location.pathname+"?nick="+nick+" class=\"btn btn-warning\" >Back to hall</a><\p>"
}
else 
{
	document.getElementById("addRoomPsw").style.display = "none";
	document.getElementById("backToHall").style.display = "none";
	document.getElementById("remRoomPsw").style.display = "none";
	document.getElementById('roomName').innerHTML = "You are in <b>Hall</b>";
}

function updateUsers(){
	if(!room)
	{
		if(users.length <= 0)
			document.getElementById('usersNames').innerHTML = "No other users";
		else
			document.getElementById('usersNames').innerHTML = "Other users:<br>"+users.toString().replace(",", "<br>");

	}

}

function backToHall()
{
		window.onbeforeunload();
		window.location.replace(location.pathname+"?nick="+nick);  
}

function setNick(f){
	f.elements['nick'].value=nick;
}

function clearChat()
{	
	bootbox.confirm("Are you sure?", function(result) {
	  if(result && isSerwerOK)
		  $.soap({
				url: 'https://sius.azurewebsites.net/services/roomServer/',
				method: 'clearRoomHistory',
				
				namespaceQualifier: 'sius',
				namespaceURL: 'http://sius.azurewebsites.net',

				data: {
					roomName: room ? room : "GlobalRoom",
				},
				
				headers: {
					'Access-Control-Allow-Origin' : '*',
				},
			});
		}); 
}

function addRoomPsw(){
bootbox.prompt("Type new password:", function(result) {                
  if (result === null) {                                                                           
  } else {
	document.getElementById("addRoomPsw").style.display = "none";
	document.getElementById("remRoomPsw").style.removeProperty('display');
    if(room && isSerwerOK)
		$.soap({
			url: 'https://sius.azurewebsites.net/services/roomServer/',
			method: 'setRoomPassword',
			
			namespaceQualifier: 'sius',
			namespaceURL: 'http://sius.azurewebsites.net',

			data: {
				roomName: room,
				password: result,
			},
			
			headers: {
				'Access-Control-Allow-Origin' : '*',
			},
		});
  }
});
}

function remRoomPsw(){
bootbox.confirm("Are you sure?", function(result) {
  if(result && isSerwerOK)
  {
	document.getElementById("addRoomPsw").style.removeProperty('display');
	document.getElementById("remRoomPsw").style.display = "none";

	$.soap({
		url: 'https://sius.azurewebsites.net/services/roomServer/',
		method: 'removeRoomPassword',
		
		namespaceQualifier: 'sius',
		namespaceURL: 'http://sius.azurewebsites.net',

		data: {
			roomName: room,
		},
		
		headers: {
			'Access-Control-Allow-Origin' : '*',
		},
	});
  }
});
}


function sendText(f){
	msg = f.elements['mgstext'].value
	if(msg!="")
	{
		document.getElementById('chatTextarea').innerHTML += buildMsg(webrtc.config.nick, msg);
		webrtc.sendDirectlyToAll("chat", "msg", {message: msg, nick: webrtc.config.nick});
		
		if(isSerwerOK)
		$.soap({
			url: 'https://sius.azurewebsites.net/services/roomServer/',
			method: 'addMessage',
			
			namespaceQualifier: 'sius',
			namespaceURL: 'http://sius.azurewebsites.net',

			data: {
				room: room ? room : "GlobalRoom",
				user: nick,
				message: msg,
			},
			
			headers: {
				'Access-Control-Allow-Origin' : '*',
			},
		});
	}
		//console.log(msg);
	f.elements['mgstext'].value = ""
	var elem = document.getElementById('chatTextarea');
	elem.scrollTop = elem.scrollHeight;

}

function funcusOnThisVideo(vid, peer){
	var remotes = document.getElementById('remotes');
	var largeVideoBox = document.getElementById('largeVideoBox')
	
	var children = largeVideoBox.children;
	for(var i = 0; i < children.length; i++)
	{
		if(children[i].id.startsWith("files_"))
			children[i].style.display = "none";
		else
			remotes.appendChild(children[i]);
	}
	//console.log();
	//console.log("---");
	//console.log("---");
	
	largeVideoBox.appendChild(vid);
	
	var fileInput = document.getElementById("files_" + vid.id);
	//fileInput.style.removeProperty.display = "";
	fileInput.style.removeProperty('display');
}