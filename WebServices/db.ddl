CREATE TABLE message
(
    id INT PRIMARY KEY NOT NULL IDENTITY,
    room VARCHAR(16) NOT NULL,
    [user] VARCHAR(16) NOT NULL,
    timestamp SMALLDATETIME NOT NULL,
    message VARCHAR(2047)
);
CREATE UNIQUE INDEX messages_id_uindex ON message (id);
CREATE TABLE room
(
    name VARCHAR(16) NOT NULL,
    description VARCHAR(255),
    password VARCHAR(16) DEFAULT NULL
);
CREATE UNIQUE INDEX room_id_uindex ON room (id);
CREATE UNIQUE INDEX room_name_uindex ON room (name);
CREATE UNIQUE INDEX room_name_pk ON room (name);
CREATE TABLE [user]
(
    name VARCHAR(16) PRIMARY KEY NOT NULL
);
CREATE UNIQUE INDEX user_name_uindex ON [user] (name);
CREATE TABLE user_room_map
(
    [user] VARCHAR(16),
    room VARCHAR(16),
    id INT PRIMARY KEY NOT NULL IDENTITY,
);
CREATE UNIQUE INDEX user_room_map_id_uindex ON user_room_map (id);