package persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by swistaq on 2016-05-28.
 */

@Entity
@Table(schema = "TurnDB.dbo", name = "[room]")
public class Room implements Serializable{

    @Id
    @Column(nullable = false, length = 16)
    private String name;

    @Column
    private String description;

    @Column
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        return name.equals(room.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
