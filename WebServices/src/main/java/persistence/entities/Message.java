package persistence.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by swistaq on 2016-06-05.
 */
@Entity
@Table(schema = "TurnDB.dbo", name = "message")
public class Message {

    @Id
    @Column
    @GeneratedValue
    private BigDecimal id;

    @Column(name = "room")
    private String room;

    @Column(name = "[user]")
    private String user;

    @Column(name = "timestamp")
    private Timestamp date;

    @Column(length = 2047)
    private String message;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return id.equals(message.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
