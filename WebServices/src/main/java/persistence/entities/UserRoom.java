package persistence.entities;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by swistaq on 2016-06-04.
 */
@Entity
@Table(schema = "TurnDB.dbo", name = "user_room_map")
public class UserRoom {

    @Id
    @Column
    @GeneratedValue
    private BigDecimal id;

    @Column(name = "[user]")
    private String user;

    @Column(name = "room")
    private String room;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRoom userRoom = (UserRoom) o;

        if (!user.equals(userRoom.user)) return false;
        return room.equals(userRoom.room);

    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + room.hashCode();
        return result;
    }
}

