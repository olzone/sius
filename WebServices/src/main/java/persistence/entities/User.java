package persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by swistaq on 2016-05-27.
 */
@Entity
@Table(schema = "TurnDB.dbo", name = "[user]")
public class User implements Serializable{

    @Id
    @Column(length = 16, nullable = false, unique = true)
    private String name;

    public User() {
    }

    public User(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}