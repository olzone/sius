package persistence.daos;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistence.HibernateUtil;
import persistence.entities.Room;
import persistence.entities.User;
import persistence.entities.UserRoom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by swistaq on 2016-05-28.
 */
public class RoomDAO implements Serializable {

    private Session session;
    private Transaction transaction;

    public List<Room> findAll() {
        List list = new ArrayList();
        try {
            openTransaction();
            list = session.createQuery("FROM Room s").list();
        } finally {
            closeTransaction();
        }
        return list;
    }

    public void userJoinRoom(String roomName, String userName) {
        try {
            openTransaction();
            UserRoom ur = new UserRoom();
            ur.setUser(userName);
            ur.setRoom(roomName);
            session.merge(ur);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeTransaction();
        }
    }

    public List<User> getUsersInRoom(String roomName){
        try {
            openTransaction();
            List<User> list = session.createQuery("From User s where s.name in (select ur.user from UserRoom ur where room=:roomName)")
                    .setParameter("roomName", roomName)
                    .list();
            return list;
        } catch (HibernateException e) {
            e.printStackTrace();
            return new ArrayList<User>();
        } finally {
            closeTransaction();
        }
    }

    public boolean isNameTaken(String name) {
        List list = new ArrayList();
        try {
            openTransaction();
            list = session.createQuery("FROM Room s WHERE s.name=:name").setParameter("name", name).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            closeTransaction();
        }
        return !list.isEmpty();
    }

    public String hasPassword(String name) {
        Room room = null;
        try {
            openTransaction();
            room = (Room) session.createQuery("From Room where name=:name").setParameter("name", name).uniqueResult();
        } catch (HibernateException e) {
            return "Unexpected error occured";
        } finally {
            closeTransaction();
        }
        if(room==null)
            return "Room does not exist";
        return room.getPassword() != null ? room.getPassword() : "false";
    }

    public String createRoom(String roomName, String description, String password) {
        try {
            openTransaction();
            Room room = new Room();
            room.setName(roomName);
            room.setDescription(description);
            if(password!=null && !password.isEmpty())
                room.setPassword(password);
            session.merge(room);
            return "Room created";
        } catch (Exception e) {
            return "Room could not be created";
        } finally {
            closeTransaction();
        }
    }

    public String setPassword(String roomName, String password) {
        try {
            openTransaction();
            Room room = new Room();
            room.setName(roomName);
            Room merged = (Room) session.merge(room);
            if (merged != null) {
                merged.setPassword(password);
                session.merge(merged);
            } else
                return "No such room";
            return "Password changed";
        } catch (Exception e) {
            return "Unexpected error occured";
        } finally {
            closeTransaction();
        }
    }

    private void openTransaction() {
        session = HibernateUtil.currentSession();
        transaction = session.beginTransaction();
    }

    private void closeTransaction() {
        transaction.commit();
        HibernateUtil.closeSession();
    }

}
