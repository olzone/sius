package persistence.daos;


import org.hibernate.Session;
import org.hibernate.Transaction;
import persistence.HibernateUtil;
import persistence.entities.Room;
import persistence.entities.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by swistaq on 2016-05-27.
 */
public class UserDAO implements Serializable {

    private Session session;
    private Transaction transaction;

    public List<User> findAll() {
        List list = new ArrayList();
        try {
            openTransaction();
            list = session.createQuery("FROM User s").list();
        } finally {
            closeTransaction();
        }
        return list;
    }

    public boolean isUsernameTaken(String username) {
        List list = new ArrayList();
        try {
            openTransaction();
            list = session.createQuery("FROM User s WHERE s.name=:name").setParameter("name", username).list();
        } finally {
            closeTransaction();
        }
        return !list.isEmpty();
    }

    public void addUser(String name) {
        try {
            openTransaction();
            User user = new User(name);
            session.merge(user);
        } finally {
            closeTransaction();
        }
    }

    public void removeUser(String userName) {
        try {
            openTransaction();
            User user = new User(userName);
            Object merged = session.merge(user);
            if(merged!=null)
                session.delete(merged);
        } finally {
            closeTransaction();
        }
    }

    public List<Room> getRoomsForUser(String userName) {
        List<Room> rooms = new ArrayList<Room>();
        try {
            openTransaction();
            rooms = session.createQuery("From Room r where r.name in (" +
                    "SELECT ur.room from UserRoom ur where ur.user=:userName" +
                    ")")
                    .setParameter("userName",userName)
                    .list();
        } finally {
            closeTransaction();
        }
        return rooms;
    }

    private void openTransaction() {
        session = HibernateUtil.currentSession();
        transaction = session.beginTransaction();
    }

    private void closeTransaction() {
        transaction.commit();
        HibernateUtil.closeSession();
    }
}
