package persistence.daos;

import org.hibernate.Session;
import org.hibernate.Transaction;
import persistence.HibernateUtil;
import persistence.entities.Message;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by swistaq on 2016-06-05.
 */
public class MessageDAO {

    private Session session;
    private Transaction transaction;

    public List<Message> findByRoom(String room) {
        openTransaction();
        List<Message> list = session.createQuery("FROM Message where room=:room").setParameter("room", room).list();
        closeTransaction();
        return list;
    }

    public void add(String room, String user, String message) {
        Message message1 = new Message();
        message1.setDate(new Timestamp(new Date().getTime()));
        message1.setRoom(room);
        message1.setUser(user);
        message1.setMessage(message);
        try {
            openTransaction();
            session.merge(message1);
        } finally {
            closeTransaction();
        }
    }

    public List<Message> findByRoomAndDate(String room, Date date) {
        List<Message> list = new ArrayList<Message>();
        try {
            openTransaction();
            list = session.createQuery("FROM Message where room=:room AND date<=:date").setParameter("room", room).setParameter("date",date).list();
        } finally {
            closeTransaction();
        }
        return list;
    }

    public List<Message> findByUser(String user) {
        List<Message> list= new ArrayList<Message>();
        try {
            openTransaction();
            list = session.createQuery("FROM Message where user=:user").setParameter("user", user).list();
        } finally {
            closeTransaction();
        }
        return list;
    }

    public void clearHistory(String room) {
        try {
            openTransaction();
            session.createQuery("DELETE FROM Message where room=:room").setParameter("room",room).executeUpdate();
        } finally {
            closeTransaction();
        }
    }

    private void openTransaction() {
        session = HibernateUtil.currentSession();
        transaction = session.beginTransaction();
    }

    private void closeTransaction() {
        transaction.commit();
        HibernateUtil.closeSession();
    }
}
