package services;


import persistence.daos.MessageDAO;
import persistence.daos.RoomDAO;
import persistence.daos.UserDAO;
import persistence.entities.Message;
import persistence.entities.Room;
import persistence.entities.User;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class WebApp {

    private UserDAO userDAO = new UserDAO();
    private RoomDAO roomDAO = new RoomDAO();
    private MessageDAO messageDAO = new MessageDAO();

    public String sayHello(String name) {
        if (name == null) {
            return "Hello";
        }
        return "Hello, " + name + "!";
    }

    public void userJoinRoom(String roomName, String userName) {
        roomDAO.userJoinRoom(roomName, userName);
    }

    public List<Room> getRoomsForUser(String userName) {
        return userDAO.getRoomsForUser(userName);
    }

    public List<User> getUsersInRoom(String roomName) {
        return roomDAO.getUsersInRoom(roomName);
    }

    public boolean isUsernameTaken(String name) {
        return userDAO.isUsernameTaken(name);
    }

    public String registerUsername(String name) {
        userDAO.addUser(name);
        return "Dodano użytkownika";
    }

    public void unregisterUsername(String name) {
        userDAO.removeUser(name);
    }

    public String isRoomPasswordProtected(String roomName) {
        return roomDAO.hasPassword(roomName);
    }

    public String setRoomPassword(String roomName, String password) {
        return roomDAO.setPassword(roomName, password);
    }

    public String removeRoomPassword(String roomName) {
        return roomDAO.setPassword(roomName, null);
    }

    public boolean isRoomNameTaken(String name) {
        return roomDAO.isNameTaken(name);
    }

    public String createRoom(String roomName, String desription) {
        return createRoomWithPassword(roomName, desription, null);
    }

    public String createRoomWithPassword(String roomName, String desription, String password) {
        return roomDAO.createRoom(roomName, desription, password);
    }

    public List<Room> getRooms() {
        return roomDAO.findAll();
    }

    public List<Message> findMessagesByRoom(String room) {
        return messageDAO.findByRoom(room);
    }

    public void addMessage(String room, String user, String message) {
        messageDAO.add(room, user, message);
    }

    public List<Message> findMessgaesByRoomAndDate(String room, String date) {
        System.out.println("\n\n\n"+date+"\n");
        Date formattedDate = new Date(Long.parseLong(date));
        System.out.println(formattedDate+"\n\n");
        return messageDAO.findByRoomAndDate(room, formattedDate);
    }

    public List<Message> findMessagesByUser(String user) {
        return messageDAO.findByUser(user);
    }

    public void clearRoomHistory(String roomName) {
        messageDAO.clearHistory(roomName);
    }

}